# -*- coding: utf-8 -*-
""" A simple Python wrapper around the Macquarie Cloud Services API """

__version__ = "0.1dev"
__author__ = "Aaron Foley"
__author_email__ = "afoley@macquariecloudservices.com"
__license__ = 'MIT'

from .manager import Manager
