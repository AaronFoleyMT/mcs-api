from distutils.core import setup

setup(
    name = 'mcs-api',
    packages = ['mcs'],
    version=mcs.__version__,
    description = 'A simple Python wrapper around the Macquarie Cloud Services API',
    author = 'Aaron Foley',
    author_email = 'afoley@macquariecloudservices.com',
    url = 'https://bitbucket.org/AaronFoleyMT/mci-api/',
    download_url = 'https://bitbucket.org/AaronFoleyMT/mci-api/get/51d150052a2b.zip',
    keywords = ['macqaurie cloud services', 'api'],
    classifiers = [],
    install_requires=[
        'markdown',
    ],
)